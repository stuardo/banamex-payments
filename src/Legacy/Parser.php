<?php
namespace Maphpia\Banamex\Legacy;

class Parser extends Connection
{
  protected $merchant;

  function __construct($merchant) {
    $this->merchant = $merchant;
    parent::__construct($merchant);
  }

  function __destruct() {
    parent::__destruct();
  }

  public function FormRequestUrl() {
    $gatewayUrl = $this->merchant->GetGatewayUrl();
    $gatewayUrl .= "/version/" . $this->merchant->GetVersion();

    $this->merchant->SetGatewayUrl($gatewayUrl);
    return $gatewayUrl;
  }

  public function ParseRequest($formData) {
    $request = "";

    if (count($formData) == 0)
      return "";

    foreach ($formData as $fieldName => $fieldValue) {
      if (strlen($fieldValue) > 0 && $fieldName != "merchant" && $fieldName != "apiPassword") {
        for ($i = 0; $i < strlen($fieldName); $i++) {
          if ($fieldName[$i] == '_')
            $fieldName[$i] = '.';
        }
        $request .= $fieldName . "=" . urlencode($fieldValue) . "&";
      }
    }

    $request .= "merchant=" . urlencode($this->merchant->GetMerchantId()) . "&";
    $request .= "apiUsername=" . urlencode($this->merchant->GetapiUsername()) . "&";
    $request .= "apiPassword=" . urlencode($this->merchant->GetPassword());

    return $request;
  }
}
