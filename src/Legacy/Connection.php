<?php
namespace Maphpia\Banamex\Legacy;

class Connection
{
  protected $curlObj;

  function __construct($merchant) {
    $this->curlObj = curl_init();
    $this->ConfigureCurlProxy($merchant);
    $this->ConfigureCurlCerts($merchant);
  }

  function __destruct() {
    curl_close($this->curlObj);
  }

  public function SendTransaction($request)
  {
    curl_setopt($this->curlObj, CURLOPT_POSTFIELDS, $request);
    curl_setopt($this->curlObj, CURLOPT_URL,        $this->merchant->GetGatewayUrl());
    curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));
    curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
    curl_setopt($this->curlObj, CURLOPT_RETURNTRANSFER, TRUE);


    curl_setopt($this->curlObj, CURLOPT_FAILONERROR, TRUE);
    curl_setopt($this->curlObj, CURLOPT_VERBOSE, TRUE);
    /*
    curl_setopt($this->curlObj, CURLOPT_HEADER, TRUE);
    curl_setopt($this->curlObj, CURLINFO_HEADER_OUT, TRUE);
    */

    $response = curl_exec($this->curlObj);

    /*
    $requestHeaders = curl_getinfo($this->curlObj);
    $response = $requestHeaders["request_header"] . $response;
    */

    if (curl_error($this->curlObj)) {
      $response = "cURL Error: " . curl_errno($this->curlObj) . " - " . curl_error($this->curlObj);
      throw new \Exception($response);
    }

    return $response;
  }

  protected function ConfigureCurlProxy() {
    if ($this->merchant->GetProxyServer() != "") {
      curl_setopt($this->curlObj, CURLOPT_PROXY, $this->merchant->GetProxyServer());
      curl_setopt($this->curlObj, $this->merchant->GetProxyCurlOption(), $this->merchant->GetProxyCurlValue());
    }

    if ($this->merchant->GetProxyAuth() != "")
      curl_setopt($this->curlObj, CURLOPT_PROXYUSERPWD, $this->merchant->GetProxyAuth());
  }

  protected function ConfigureCurlCerts() {
    if ($this->merchant->GetCertificatePath() != "")
      curl_setopt($this->curlObj, CURLOPT_CAINFO, $this->merchant->GetCertificatePath());

    curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYPEER, $this->merchant->GetCertificateVerifyPeer());
    curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYHOST, $this->merchant->GetCertificateVerifyHost());
  }
}
