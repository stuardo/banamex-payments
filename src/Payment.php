<?php
namespace Maphpia\Banamex;

class Payment
{
    // Payment Status
    const PENDING        = 'pending';
    const COMPLETED      = 'authorized';
    const DECLINED       = 'declined';
    const CANCELLED      = 'cancelled';
    const REFUNDED       = 'refunded';
    const REQUEST_REFUND = 'request_refund';

    public $logArray = [];

    private function _tns()
    {
        require_once 'checkout-tns-process/Connection.php';
        require_once 'checkout-tns-process/Merchant.php';
        require_once 'checkout-tns-process/Parser.php';
        $tns = [
            'certificateVerifyPeer' => false,
            'certificateVerifyHost' => false,
            'debug'                 => false,
            'version'               => 30,
            'gatewayUrl'            => 'https://secure.na.tnspayments.com/api/nvp',
            'merchantId'            => env('TNS_MERCHANT_ID'),
            'apiUsername'           => 'merchant.'.env('TNS_USER'),
            'password'              => env('TNS_API_KEY'),
        ];
        $merchant  = new Legacy\Merchant($tns);
        $parser    = new Legacy\Parser($merchant);
        return $parser;
    }

    /**
     * Make the call to the API to create an order to pay
     *
     * @return void
     */
    public function prepare(Order $order)
    {
        $orderReferenceId = $order->referenceId;
        $total            = CartItem::getGrandTotal('MXN');
        $protocol         = (isset($_SERVER['HTTPS']) && 'on' === $_SERVER['HTTPS']) ? 'https' : 'http';

        $orderData = [
            'apiOperation'                              => 'CREATE_CHECKOUT_SESSION',
            'transaction_reference'                     => $orderReferenceId,
            'order_id'                                  => $orderReferenceId,
            'order_reference'                           => $orderReferenceId,
            'order_amount'                              => $total,
            'order_currency'                            => 'MXN',
            'interaction_returnUrl'                     => "$protocol://{$_SERVER['SERVER_NAME']}/payment/success",
            'interaction_displayControl_billingAddress' => 'HIDE',
            'interaction_displayControl_customerEmail'  => 'HIDE',
            'interaction_displayControl_orderSummary'   => 'HIDE',
            'interaction_displayControl_paymentTerms'   => 'HIDE',
            'interaction_displayControl_shipping'       => 'HIDE',
            'interaction_locale'                        => $order->group->language,
            'shipping_method'                           => 'SAME_DAY',
        ];
        $order->log('prepare', $orderData);

        $tns        = $this->_tns();
        $request    = $tns->ParseRequest($orderData);
        $requestUrl = $tns->FormRequestUrl();
        $response   = $tns->SendTransaction($request);
        parse_str($response, $response);
        $order->log('prepareResponse', $response);
        return $response;
    }

    public function parseResponse(Order $order)
    {
        $tns = $this->_tns();
        $requestString = array("apiOperation"=>"RETRIEVE_ORDER", "order.id"=>$order->referenceId);
        $request       = $tns->ParseRequest($requestString);
        $requestUrl    = $tns->FormRequestUrl();
        $response      = $tns->SendTransaction($request);

        $parsed     = urldecode($response);
        $parsed     = str_replace(['[0', ']'], '', $parsed);
        $parsed     = str_replace(['[1', ']'], '', $parsed);
        parse_str($parsed, $parsed);
        $order->log('retrieveOrder', $parsed);

        $token = [
            'amount'        => $parsed['amount'],
            'authorization' => $parsed['transaction_transaction_authorizationCode'],
            'cc_month'      => $parsed['sourceOfFunds_provided_card_expiry_month'],
            'cc_name'       => $parsed['sourceOfFunds_provided_card_nameOnCard'],
            'cc_number'     => $parsed['sourceOfFunds_provided_card_number'],
            'cc_type'       => $parsed['sourceOfFunds_provided_card_brand'],
            'cc_year'       => $parsed['sourceOfFunds_provided_card_expiry_year'],
            'created_at'    => $parsed['creationTime'],
            'currency'      => $parsed['currency'],
            'id'            => $parsed['id'],
            'ip'            => $parsed['device_ipAddress'],
            'receipt'       => $parsed['transaction_transaction_receipt'],
            'response'      => (isset($parsed['transaction_response_acquirerMessage']))
                            ? $parsed['transaction_response_acquirerMessage']
                            : $parsed['transaction_response_gatewayCode'],
            'result'        => $parsed['transaction_result'],
            'status'        => $parsed['status'],
            'total'         => $parsed['totalAuthorizedAmount'],
        ];
        $order->log('token', $token);
        if ( ! (int) $token['authorization']) {
            $this->logArray = $parsed;
            throw new \Exception("Invalid authorization code: {$token['authorization']}", $token['authorization']);
        }
        return $token;
    }

    public function wasAlreadyApproved($order)
    {
        $tns = $this->_tns();
        $requestString = array("apiOperation"=>"RETRIEVE_ORDER", "order.id"=>$order->referenceId);
        $request       = $tns->ParseRequest($requestString);
        $requestUrl    = $tns->FormRequestUrl();
        $response      = $tns->SendTransaction($request);
        parse_str($response, $response);
        return ($response['result'] == 'SUCCESS');
    }
}