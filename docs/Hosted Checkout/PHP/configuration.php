<?php

$configArray = array();

/*
 Establezca toda su configuraci�n en este archivo

 El ajuste de configuraci�n de debug es �til para imprimir los requests y responses
 En caso de recibir alg�n error o respuesta inesperada, establezca esta bandera a TRUE
 
 La salida del debug le ayudar� a identificar la causa del problema
 
  
  Por favor comente la configuraci�n del proxy si usted no desea utilizar un proxy
 
*/

// Si utiliza un servidor proxy, quite el comentario de las siguientes configuraciones de proxy

// Nombre del Servidor o direcci�n IP y puerto de su servidor proxy
//$configArray["proxyServer"] = "server:port";

// Nombre de usuario y contrase�a para la autenticaci�n del servidor proxy
//$configArray["proxyAuth"] = "username:password";

// El siguiente valor no se debe cambiar
//$configArray["proxyCurlOption"] = CURLOPT_PROXYAUTH;

// Tipo de CURL Proxy. Los valores soportados actualmente: CURLAUTH_NTLM y CURLAUTH_BASIC 
//$configArray["proxyCurlValue"] = CURLAUTH_NTLM;


// Si se usa la validaci�n de certificados, modificar los siguientes ajustes de configuraci�n

// archivo alternativo de certificado de confianza
// dejar como "" si no cuenta con una ruta para el certificado
//$configArray["certificatePath"] = "C:/ca-cert-bundle.crt";

// posibles valores:
// FALSE = deshabilita la verificaci�n
// TRUE = habilita la verificaci�n
$configArray["certificateVerifyPeer"] = FALSE;

// posibles valores:
// 0 = no comprueba/verifica el hostname
// 1 = comprueba la existencia del hostname en el certificado
// 2 = para verificar que el hostname coinicide con el proporcionado
$configArray["certificateVerifyHost"] = 0;


// URL Base del Payment Gateway. No incluya la versi�n.
//$configArray["gatewayUrl"] = "https://[INSERT-DOMAIN]/api/nvp";
$configArray["gatewayUrl"] = "https://secure.na.tnspayments.com/api/nvp";

// Merchant ID proporcionado por Servicios de Pago Banamex
//$configArray["merchantId"] = "[Ingrese su Merchant ID]";
$configArray["merchantId"] = "TESTVENTA_TEST";

// Nombre de usuario API en el formato de abajo donde el Merchant ID es el mismo que el anterior
//$configArray["apiUsername"] = "merchant.[Ingrese su Merchant ID]";
$configArray["apiUsername"] = "merchant.TESTVENTA_TEST";

// Contrase�a del API que se obtiene del Merchant Administration
//$configArray["password"] = "[Ingrese su API password]";
$configArray["password"] = "08594fad7c0fce7d47ee565963186d00";

// Controles de configuraci�n de debug que muestran el contenido en bruto
// del request y del response para una transacci�n.
// En producci�n debe garantizar que esto se establezca en FALSO para no
// visualizar/usar esta informaci�n de debug.
$configArray["debug"] = FALSE;

// N�mero de versi�n de la API que est� utilizando para su integraci�n
// este es el valor por defecto si no se ha especificado en process.php
$configArray["version"] = "30";


/* 	
 Esta clase contiene todas las variables relacionadas con el comercio y 
 ajustes de configuraci�n del proxy.	
*/
class Merchant {
	private $proxyServer = "";
	private $proxyAuth = "";
	private $proxyCurlOption = 0;
	private $proxyCurlValue = 0;	
	
	private $certificatePath = "";
	private $certificateVerifyPeer = FALSE;	
	private $certificateVerifyHost = 0;

	private $gatewayUrl = "";
	private $debug = FALSE;
	private $version = "";
	private $merchantId = "";
	private $apiUsername = "";
	private $password = "";
	
	
	/*
	 El constructor toma un config array. La estructura de este array se define
	 en la parte superior de esta p�gina
	*/
	function __construct($configArray) {
		if (array_key_exists("proxyServer", $configArray))
			$this->proxyServer = $configArray["proxyServer"];
		
		if (array_key_exists("proxyAuth", $configArray))
			$this->proxyAuth = $configArray["proxyAuth"];
			
		if (array_key_exists("proxyCurlOption", $configArray))
			$this->proxyCurlOption = $configArray["proxyCurlOption"];
		
		if (array_key_exists("proxyCurlValue", $configArray))
			$this->proxyCurlValue = $configArray["proxyCurlValue"];
			
		if (array_key_exists("certificatePath", $configArray))
			$this->certificatePath = $configArray["certificatePath"];
			
		if (array_key_exists("certificateVerifyPeer", $configArray))
			$this->certificateVerifyPeer = $configArray["certificateVerifyPeer"];
			
		if (array_key_exists("certificateVerifyHost", $configArray))
			$this->certificateVerifyHost = $configArray["certificateVerifyHost"];
		
		if (array_key_exists("gatewayUrl", $configArray))
			$this->gatewayUrl = $configArray["gatewayUrl"];
		
		if (array_key_exists("debug", $configArray))	
			$this->debug = $configArray["debug"];
			
		if (array_key_exists("version", $configArray))
			$this->version = $configArray["version"];
			
		if (array_key_exists("merchantId", $configArray))	
			$this->merchantId = $configArray["merchantId"];
			
		if (array_key_exists("apiUsername", $configArray))	
			$this->apiUsername = $configArray["apiUsername"];
		
		if (array_key_exists("password", $configArray))
			$this->password = $configArray["password"];
			
	}
	
	// M�toros Get para devolver un valor espec�fico
	public function GetProxyServer() { return $this->proxyServer; }
	public function GetProxyAuth() { return $this->proxyAuth; }
	public function GetProxyCurlOption() { return $this->proxyCurlOption; }
	public function GetProxyCurlValue() { return $this->proxyCurlValue; }
	public function GetCertificatePath() { return $this->certificatePath; }
	public function GetCertificateVerifyPeer() { return $this->certificateVerifyPeer; }
	public function GetCertificateVerifyHost() { return $this->certificateVerifyHost; }
	public function GetGatewayUrl() { return $this->gatewayUrl; }
	public function GetDebug() { return $this->debug; }
	public function GetVersion() { return $this->version; }	
	public function GetMerchantId() { return $this->merchantId; }
	public function GetapiUsername() { return $this->apiUsername; }
	public function GetPassword() { return $this->password; }
	
	// M�todos Set para establecer un valor
	public function SetProxyServer($newProxyServer) { $this->proxyServer = $newProxyServer; }
	public function SetProxyAuth($newProxyAuth) { $this->proxyAuth = $newProxyAuth; }
	public function SetProxyCurlOption($newCurlOption) { $this->proxyCurlOption = $newCurlOption; }
	public function SetProxyCurlValue($newCurlValue) { $this->proxyCurlValue = $newCurlValue; }
	public function SetCertificatePath($newCertificatePath) { $this->certificatePath = $newCertificatePath; }
	public function SetCertificateVerifyPeer($newVerifyHostPeer) { $this->certificateVerifyPeer = $newVerifyHostPeer; }
	public function SetCertificateVerifyHost($newVerifyHostValue) { $this->certificateVerifyHost = $newVerifyHostValue; }
	public function SetGatewayUrl($newGatewayUrl) { $this->gatewayUrl = $newGatewayUrl; }
	public function SetDebug($debugBool) { $this->debug = $debugBool; }
	public function SetVersion($newVersion) { $this->version = $newVersion; }
	public function SetMerchantId($merchantId) {$this->merchantId = $merchantId; }
	public function SetapiUsername($apiUsername) {$this->apiUsername = $apiUsername; }
	public function SetPassword($password) { $this->password = $password; }
}

?>