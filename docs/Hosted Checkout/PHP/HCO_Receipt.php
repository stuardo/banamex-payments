<?php
session_start();
if($_SESSION['successIndicator'] == $_GET['resultIndicator'] AND isset($_SESSION['order.id'])){
//if(($_SESSION['order.id'])){
include "configuration.php";
include "connection.php";

// Crea un objeto Merchant del archivo config.  
$merchantObj = new Merchant($configArray);

// El objeto Parser se utiliza para procesar la respuesta del gateway y manejar las conexiones
$parserObj = new Parser($merchantObj);

$requestString  = array("apiOperation"=>"RETRIEVE_ORDER","order.id"=>$_SESSION['order.id']);


// forme el request de la transacci�n
//$request = $parserObj->ParseRequest($merchantObj, $GET);
//echo $requestString;
$request = $parserObj->ParseRequest($merchantObj, $requestString);


// En caso de no recibir un GET de la p�gina HTML (parseRequest devolver� "" al recibir un $_GET vac�o)
if ($request == "")
	die();

// imprime la solicitud antes del env�o al servidor en caso de est�r en modo debug
// esto �nicmente se utiliza para depurar. Esto no se debe utilizar en su integraci�n, ya que DEBUG debe establecerse como FALSE
if ($merchantObj->GetDebug())
	echo $request . "<br/><br/>";

// forma el requestUrl y lo asigna al miembro merchantObj gatewayUrl
// devuelve lo que estaba asignado al miembro gatewayUrl para imprimirlo si est� en modo de depuraci�n
$requestUrl = $parserObj->FormRequestUrl($merchantObj);

// esto �nicmente se utiliza para depurar. Esto no se debe utilizar en su integraci�n, ya que DEBUG debe establecerse como FALSE
if ($merchantObj->GetDebug())
	echo $requestUrl . "<br/><br/>";
	
// se realiza el intento de transacci�n
// $response se utiliza en la p�gina de recibo, no cambie nombre de la variable
$response = $parserObj->SendTransaction($merchantObj, $request);

// imprime la respuesta recibida del server
if ($merchantObj->GetDebug()) {
	// remplaza los caracteres de  newline chars con caracteres newlines de html 
	$response = str_replace("\n", "<br/>", $response);
	echo $response . "<br/><br/>";
	die();
}
}
include "receipt.php";

?>
