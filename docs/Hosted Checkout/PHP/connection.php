<?php

class Connection {
  protected $curlObj;

  function __construct($merchantObj) {
    // inicializa cURL object/options
    $this->curlObj = curl_init();

    // configure las opciones del proxy cURL llamando a esta funci�n
    $this->ConfigureCurlProxy($merchantObj);

    // configure las opciones de verificaci�n del certificado cURL llamando a esta funci�n
    $this->ConfigureCurlCerts($merchantObj);
  }

  function __destruct() {
    // libere cURL resources/session
    curl_close($this->curlObj);
  }

  // Env�a la transacci�n al payment server
  public function SendTransaction($merchantObj, $request) {
    // [Snippet] howToPost - start
    curl_setopt($this->curlObj, CURLOPT_POSTFIELDS, $request);
    // [Snippet] howToPost - end

    // [Snippet] howToSetURL - start
    curl_setopt($this->curlObj, CURLOPT_URL, $merchantObj->GetGatewayUrl());
		// [Snippet] howToSetURL - end

    // [Snippet] howToSetHeaders - start
    // establece la longitud del contenido del encabezado HTTP
    curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));

    // establece el conjunto de caracteres a UTF-8 (requisito del payment server)
    curl_setopt($this->curlObj, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
    // [Snippet] howToSetHeaders - end

    // le indica a cURL regresar el resultado
    curl_setopt($this->curlObj, CURLOPT_RETURNTRANSFER, TRUE);

    // esto �nicmente se utiliza para depurar. Esto no se debe utilizar en su integraci�n, ya que DEBUG debe establecerse como FALSE
    if ($merchantObj->GetDebug()) {
      curl_setopt($this->curlObj, CURLOPT_HEADER, TRUE);
      curl_setopt($this->curlObj, CURLINFO_HEADER_OUT, TRUE);
    }

    // [Snippet] executeSendTransaction - start
    // env�a la transacci�n
    $response = curl_exec($this->curlObj);
    // [Snippet] executeSendTransaction - end

    // esto �nicmente se utiliza para depurar. Esto no se debe utilizar en su integraci�n, ya que DEBUG debe establecerse como FALSE
    if ($merchantObj->GetDebug()) {
      $requestHeaders = curl_getinfo($this->curlObj);
      $response = $requestHeaders["request_header"] . $response;
    }

    // asigna el error cURL a la respuesta si algo sali� mal por lo que el solicitante puede ver dicho error
    if (curl_error($this->curlObj))
      $response = "cURL Error: " . curl_errno($this->curlObj) . " - " . curl_error($this->curlObj);

    // responder con el resultado de la transacci�n, o un mensaje de error cURL si algo fall�
    return $response;
  }

  // [Snippet] howToConfigureProxy - start
  // Comprueba si est� deinida la configuraci�n del Proxy, en caso afirmativo se configura el objeto cURL
  protected function ConfigureCurlProxy($merchantObj) {
    // Si est� definido el proxy server, se establece la opci�n en cURL
    if ($merchantObj->GetProxyServer() != "") {
      curl_setopt($this->curlObj, CURLOPT_PROXY, $merchantObj->GetProxyServer());
      curl_setopt($this->curlObj, $merchantObj->GetProxyCurlOption(), $merchantObj->GetProxyCurlValue());
    }

    // Si la autenticaci�n del proxy, se establece la opci�n en cURL
    if ($merchantObj->GetProxyAuth() != "")
      curl_setopt($this->curlObj, CURLOPT_PROXYUSERPWD, $merchantObj->GetProxyAuth());
  }
  // [Snippet] howToConfigureProxy - end

  // [Snippet] howToConfigureSslCert - start
  // configure los ajustes relativos a la verificaci�n del certificado en el objeto cURL
  protected function ConfigureCurlCerts($merchantObj) {
    // si el usuario ha dado una ruta para un certificado, se establece en el objeto cURL para comprobarlo
    if ($merchantObj->GetCertificatePath() != "")
      curl_setopt($this->curlObj, CURLOPT_CAINFO, $merchantObj->GetCertificatePath());

    curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYPEER, $merchantObj->GetCertificateVerifyPeer());
    curl_setopt($this->curlObj, CURLOPT_SSL_VERIFYHOST, $merchantObj->GetCertificateVerifyHost());
  }
  // [Snippet] howToConfigureSslCert - end

}



class Parser extends Connection {
  function __construct($merchantObj) {
    // llama al constructor de Connection para inicializar los miembros
    parent::__construct($merchantObj);
  }

  function __destruct() {
    // llama al destructor de Connection para liberar recursos
    parent::__destruct();
  }

	// [Snippet] howToConfigureURL - start
  // Modifica la URL del gateway para establecer la versi�n
  // Lo asigna al miembro gatewayUrl en el objeto merchantObj
  public function FormRequestUrl($merchantObj) {
    $gatewayUrl = $merchantObj->GetGatewayUrl();
    $gatewayUrl .= "/version/" . $merchantObj->GetVersion();

    $merchantObj->SetGatewayUrl($gatewayUrl);
    return $gatewayUrl;
  }
  // [Snippet] howToConfigureURL - end

  // [Snippet] howToConvertFormData - start
  // Forme un request con formato NVP y agregue merchantId, apiUsername y apiPassword
  public function ParseRequest($merchantObj, $formData) {
    $request = "";

    if (count($formData) == 0)
      return "";

    foreach ($formData as $fieldName => $fieldValue) {
      if (strlen($fieldValue) > 0 && $fieldName != "merchant" && $fieldName != "apiPassword") {
        // reemplaza los guiones bajos en los nombres de campo por punto
        for ($i = 0; $i < strlen($fieldName); $i++) {
          if ($fieldName[$i] == '_')
            $fieldName[$i] = '.';
        }
        $request .= $fieldName . "=" . urlencode($fieldValue) . "&";
      }
    }

    // [Snippet] howToSetCredentials - start
    // Para NVP, los detalles de autenticaci�n se pasan en el cuerco como Name-Value-Pairs, al igual que cualquier otro campo de datos
    $request .= "merchant=" . urlencode($merchantObj->GetMerchantId()) . "&";
    $request .= "apiUsername=" . urlencode($merchantObj->GetapiUsername()) . "&";
    $request .= "apiPassword=" . urlencode($merchantObj->GetPassword());
    // [Snippet] howToSetCredentials - end

    return $request;
  }
  // [Snippet] howToConvertFormData - end
}

?>