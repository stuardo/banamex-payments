<?php
function curPageURL() {
 $pageURL = 'http';
 if (isset($_SERVER["HTTPS"])) {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"]. dirname($_SERVER["REQUEST_URI"]);
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"]. dirname($_SERVER["REQUEST_URI"]);
 }
 return $pageURL;
}

function getGUID(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
        return $uuid;
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

  <link rel="stylesheet" type="text/css" href="./assets/paymentstyle.css" />

  <head>
    <title>C&oacute;digo de Ejemplo API</title>
    <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
  </head>

  <body>

      <h1>Ejemplo PHP - Hosted Checkout con Retorno</h1>
      <h3>Operaci&oacute;n de Pago</h3>
      

    <form action="./process.php" method="post">

    <table width="80%" align="center" cellpadding="5" border="0">

      <!-- Campos de la Tarjeta de Cr�dito -->
      	<tr>
      	<td colspan="2">
      	El modelo Hosted Checkout le permite recolectar los datos de pago de su cliente a trav&eacute;s de una interacci&oacute;n alojada y desplegada por el Gateway de Pagos.
      	Con este modelo de integraci&oacute;n, usted nunca ve o maneja los datos de la tarjeta directamente debido a que son recolectados por la interfaz de pago que administra el Gateway de Pagos.
        Este ejemplo est&aacute; implementado con un Lightbox que es una ventana de dialogo sobre la parte de su tienda virtual.
        </td>
      	</tr>
         <tr class="title">
             <td colspan="2" height="25"><P><strong>Campos necesarios para realizar la transacci&oacute;n</strong></P></td>
         </tr>

         <tr>
             <td colspan="2" height="25"><P class="desc">El campo Order ID es obligatorio. Para este ejemplo, se utiliza la funci�n getGUID declarada en el c&oacute;digo, en su integraci&oacute;n, usted debe generar este campo dentro de su c&oacute;digo y no los mostrar&aacute; al tarhetahabiente en esta p&aacute;gina, ni tampoco lo pasar&aacute; como campo oculto. </P></td>
         </tr>

         
         <tr class="shade">
             <td align="right" width="60%"><strong>order.id </strong><br>Identificador &uacute;nico de la orden para distinguirla de cualquier otra orden que genere.</td>
             <td width="40%"><input type="text" name="order.id" value=<?php echo(getGUID()); ?> size="60" maxlength="60"/></td>
         </tr>

         <tr>
             <td align="right" width="60%"><strong>apiOperation </strong></td>
             <td width="40%"><input type="text" readonly="readonly" name="apiOperation" value="CREATE_CHECKOUT_SESSION" size="30" maxlength="80"/></td>
         </tr>

         <tr class="shade">
             <td align="right" width="60%"><strong>order.amount</strong><br>Monto de la transacci&oacute;n</td>
             <td width="40%"><input type="text" name="order.amount" value="100" size="8" maxlength="13"/></td>
         </tr>

         <tr>
             <td align="right" width="60%"><strong>order.currency </strong><br>Moneda de la transacci&oacute;n</td>
             <td width="40%"><input type="text" name="order.currency" value="MXN" size="8" maxlength="3"/></td>
         </tr>

         <tr class="shade">
             <td align="right" width="60%"><strong>transaction.reference </strong><br>Referencia para la transacci&oacute;n</td>
             <td width="40%"><input type="text" name="transaction.reference" value="" size="40" maxlength="40"/></td>
         </tr>

         <tr>
             <td align="right" width="60%"><strong>order.reference </strong><br>El identificador de la orden. Por ejemplo, un n&uacute;mero de carrito de compra, un n&uacute;mero de orden, o un n&uacute;mero de factura.</td>
             <td width="40%"><input type="text" name="order.reference" value="" size="40" maxlength="40"/></td>
         </tr>
         </table>
         <hr>
         <table width="80%" align="center" cellpadding="5" border="0">
         <tr class="title">
         	 <td colspan="2" height="25"><P><strong>Los siguientes campos son opcionales dado que manejan un valor por defecto. S&oacute;lo los campos en rojo son obligatorios</strong></P></td>
         </tr>
         <tr class="title">
             <td colspan="2" height="25"><P><strong>Campos para controlar la interacci&oacute;n del tarjetahabiente en el checkout, existen otros campos que se configuran en el archivo configuration.php</strong></P></td>
         </tr>

         <tr class="shade">
             <td align="right" width="60%"><strong><P class="red">interaction.cancelUrl</p></strong>La URL a la que desea redirigir el navegador del tarjetahabiente si cancela su pago.</td>
             <td width="40%"><input type="text" name="interaction.cancelUrl" value=<?php echo(curPageURL() . '/cancel.html'); ?> size="60"/></td>
         </tr>
         <tr>
             <td align="right" width="60%"><strong><P class="red">interaction.returnUrl</p></strong>La URL a la que se desea redirigir al tarjetahabiente despu�s de terminar el intento de pago. Durante la redirecci�n, el gateway a�adir� el par�metro resultIndicator a esta URL. Este par�metro determina el resultado del pago.</td>
             <td width="40%"><input type="text" name="interaction.returnUrl" value=<?php echo(curPageURL() . '/HCO_Receipt.php'); ?> size="60"/></td>
         </tr>
         <tr class="shade">
             <td align="right" width="60%"><strong>interaction.displayControl.billingAddress</strong><br>Indica si se le mostrar&aacute; al tarjetahabiente los campos para proporcionar su direcci&oacute;n de facturaci&oacute;n en el lightbox. Si usted no proporciona este campo, la direcci&oacute;n de facturaci&oacute;n ser&aacute; opcional</td>
             <td width="40%">
             <select name="interaction.displayControl.billingAddress">
             	<option value ="" selected>Opcional</option>
             	<option value ="HIDE">No mostrar</option>
             	<option value ="MANDATORY">Muestra los campos como obligatorios</option>
             	<option value ="OPTIONAL">Muestra los campos como opcionales</option>
             	<option value ="READ_ONLY">Se muestran los datos pero no se pueden modificar</option>
             </select>
             </td>
         </tr>
         
         <tr>
             <td align="right" width="60%"><strong>interaction.displayControl.customerEmail</strong><br>Indica si se le solicitar&aacute; al tarjetahabiente que proporcione su email en el lightbox. Si usted no proporciona este campo, el email se ocultar&aacute;</td>
             <td width="40%">
             <select name="interaction.displayControl.customerEmail">
             	<option value ="" selected>Por Defecto</option>
             	<option value ="HIDE">No mostrar</option>
             	<option value ="MANDATORY">Muestra los campos como obligatorios</option>
             	<option value ="OPTIONAL">Muestra los campos como opcionales</option>
             	<option value ="READ_ONLY">Se muestran los datos pero no se pueden modificar</option>
             </select>
             </td>
         </tr>
         
         <tr class="shade">
             <td align="right" width="60%"><strong>interaction.displayControl.orderSummary</strong><br>Indica si desea mostrar un resumen de la orden. El resumen de la orden se muestra si usted no proporciona este campo.</td>
             <td width="40%">
             <select name="interaction.displayControl.orderSummary">
             	<option value ="" selected>Mostrar</option>
             	<option value ="HIDE">No mostrar</option>
             	<option value ="READ_ONLY">Se muestran los datos pero no se pueden modificar</option>
             </select>
             </td>
         </tr>
         
         <tr>
             <td align="right" width="60%"><strong>interaction.displayControl.paymentTerms</strong><br>Indica si desea ocultar las condiciones de meses sin intereses en el lightbox. Si usted no proporciona este campo se mostrar&aacute;n las opciones de mensualidades que tenga configuradas.</td>
             <td width="40%">
             <select name="interaction.displayControl.paymentTerms">
             	<option value ="" selected>Por Defecto</option>
             	<option value ="HIDE">No mostrar</option>
             	<option value ="SHOW_IF_SUPPORTED">Se muestran los planes si est&aacute;n configurados</option>
             </select>
             </td>
         </tr>
         
         <tr class="shade">
             <td align="right" width="60%"><strong>interaction.displayControl.shipping</strong><br>Indica si desea ocultar la direcci&oacute;n de env�o en el lightbox. Si usted no proporciona este campo, se mostrar&aacute; al tarjetahabiente.</td>
             <td width="40%">
             <select name="interaction.displayControl.shipping">
             	<option value ="" selected>Mostrar</option>
             	<option value ="HIDE">No mostrar</option>
             	<option value ="READ_ONLY">Se muestran los datos pero no se pueden modificar</option>
             </select>
             </td>
         </tr>
         </table>
         <hr>
         <table width="80%" align="center" cellpadding="5" border="0">
         <!-- Campos requeridos para Accertify -->
         <tr class="title">
             <td colspan="2" height="25"><P><strong>Los siguientes campos son necesarios si usted contrat&oacute; el M&oacute;dulo de Riesgo de Accertify, en caso contrario puede dejarlos en blanco.</strong></P></td>
         </tr>
         <tr class="title">
             <td colspan="2" height="25"><P><strong>Campos para la validaci&oacute;n del M&oacute;dulo de Riesgo de Accertify</strong></P></td>
         </tr>
         <tr class="title">
             <td colspan="2" height="25"><P class="desc"><strong>Datos del Cliente</strong></P></td>
         </tr>
         <tr class="shade">
             <td align="right" width="60%"><strong>customer.email</strong><br>La direcci&oacute;n de correo electr&oacute;nico del cliente</td>
             <td width="40%"><input type="text" name="customer.email" value="" size="40" maxlength="80"/></td>
         </tr>

         <tr>
             <td align="right" width="60%"><strong>customer.firstName</strong> <br>Nombre del Cliente</td>
             <td width="40%"><input type="text" name="customer.firstName" value="" size="30" maxlength="50"/></td>
         </tr>
         
         <tr class="shade">
             <td align="right" width="60%"><strong>customer.lastName</strong><br>Apellido del Cliente</td>
             <td width="40%"><input type="text" name="customer.lastName" value="" size="30" maxlength="50"/></td>
         </tr>

         <tr>
             <td align="right" width="60%"><strong>customer.phone</strong> <br>Tel&eacute;fono del Cliente</td>
             <td width="40%"><input type="text" name="customer.phone" value="" size="10" maxlength="20"/></td>
         </tr>
         <!-- La siguiente secci�n es para solicitar los datos de la Direcci�n de Facturaci�n, si es que usted desea obtener directamente
         los datos y pasarlos al Payment Gateway al solicitar la sesi�n -->
         <!--
         <tr class="title">
             <td colspan="2" height="25"><P class="desc"><strong>Datos de la Direcci&oacute;n de Facturaci&oacute;n</strong></P></td>
         </tr>
         <tr class="shade">
             <td align="right" width="60%"><strong>billing.address.street</strong><br>La primer l&iacute;nea de la direcci&oacute;n del cliente. Ej, Nombre de la calle y n&uacute;mero</td>
             <td width="40%"><input type="text" name="billing.address.street" value="" size="50" maxlength="100"/></td>
         </tr>

         <tr>
             <td align="right" width="60%"><strong>billing.address.street2</strong> <br>La segunda l&iacute;nea de la direcci&oacute;n, Ej, Colonia</td>
             <td width="40%"><input type="text" name="billing.address.street2" value="" size="50" maxlength="100"/></td>
         </tr>
         
         <tr class="shade">
             <td align="right" width="60%"><strong>billing.address.city</strong><br>Ciudad de la direcci&oacute;n</td>
             <td width="40%"><input type="text" name="billing.address.city" value="" size="50" maxlength="100"/></td>
         </tr>

         <tr>
             <td align="right" width="60%"><strong>billing.address.stateProvince</strong> <br>El estado o provincia de la direcci&oacute;n.</td>
             <td width="40%"><input type="text" name="billing.address.stateProvince" value="" size="50" maxlength="100"/></td>
         </tr>
         
         <tr class="shade">
             <td align="right" width="60%"><strong>billing.address.postcodeZip</strong><br>C&oacute;digo Postal de la direcci&oacute;n</td>
             <td width="40%"><input type="text" name="billing.address.postcodeZip" value="" size="10" maxlength="10"/></td>
         </tr>
         -->
         <tr class="title">
             <td colspan="2" height="25"><P class="desc"><strong>Datos de la Direcci&oacute;n de Envio</strong></P></td>
         </tr>
         <tr class="shade">
             <td align="right" width="60%"><strong>shipping.address.street</strong><br>La primer l&iacute;nea de la direcci&oacute;n. Ej, Nombre de la calle y n&uacute;mero</td>
             <td width="40%"><input type="text" name="shipping.address.street" value="" size="50" maxlength="100"/></td>
         </tr>

         <tr>
             <td align="right" width="60%"><strong>shipping.address.street2</strong> <br>La segunda l&iacute;nea de la direcci&oacute;n, Ej, Colonia</td>
             <td width="40%"><input type="text" name="shipping.address.street2" value="" size="50" maxlength="100"/></td>
         </tr>
         
         <tr class="shade">
             <td align="right" width="60%"><strong>shipping.address.city</strong><br>Ciudad de la direcci&oacute;n</td>
             <td width="40%"><input type="text" name="shipping.address.city" value="" size="50" maxlength="100"/></td>
         </tr>

         <tr>
             <td align="right" width="60%"><strong>shipping.address.stateProvince</strong> <br>El estado o provincia de la direcci&oacute;n.</td>
             <td width="40%"><input type="text" name="shipping.address.stateProvince" value="" size="50" maxlength="100"/></td>
         </tr>
         
         <tr class="shade">
             <td align="right" width="60%"><strong>shipping.address.postcodeZip</strong><br>C&oacute;digo Postal de la direcci&oacute;n de Facturaci&oacute;n</td>
             <td width="40%"><input type="text" name="shipping.address.postcodeZip" value="" size="10" maxlength="10"/></td>
         </tr>
         
         <tr>
             <td align="right" width="60%"><strong>shipping.method</strong> <br>El c&oacute;digo del m&eacute;todo de env&iacute;o para indicar el per&iacute;odo de tiempo y la prioridad de la orden</td>
             <td width="40%">
             <select name="shipping.method">
             	<option value ="SAME_DAY">Mismo d&iacute;a</option>
             	<option value ="OVERNIGHT" selected>Al d&iacute;a siguiente </option>
             	<option value ="PRIORITY">Prioridad (2-3 d&iacute;as)</option>
             	<option value ="GROUND">4 o m&aacute;s d&iacute;as</option>
             	<option value ="ELECTRONIC">Entrega electr&oacute;nica</option>
             </select>
             </td>
         </tr>
         
         
         <tr>
              <td colspan="2"><center><input type="submit" value="Procesar Pago"/></center></td>
         </tr>

    </table>

    </form>
    <br/><br/>

    </body>
</html>
