<?php

/* P�gina principal de control

1. Crear 1 objeto MerchantConfiguration para cada merchant ID
2. Crear 1 objeto Parser
3. Llamar al metodo FormRequest del objeto Parser para formar el request que ser� enviado al payment server
4. Analice gram�ticamente el reqest formado para llamar al m�todo SendTransaction para tratar de enviar la transacci�n al payment server
5. Guarde la respuesta transacci�n recibida en una variable
6. Incluya la p�gina de recibo la cual ser� la salida HTML de la respueta y analice la respuesta del payment server.

*/
session_start();
include "configuration.php";
include "connection.php";
// Quite el bot�n submit para que bno sea enviado en la petici�n POST
if (array_key_exists("submit", $_POST))
	unset($_POST["submit"]);
	$_SESSION['order.id'] = $_POST["order_id"];
	
	
// Crea un objeto Merchant del archivo config.  
$merchantObj = new Merchant($configArray);

// El objeto Parser se utiliza para procesar la respuesta del gateway y manejar las conexiones
$parserObj = new Parser($merchantObj);

// En su integraci�n, no deber� pasar este dato, solo almacene el valor en el archivo configuration.
if (array_key_exists("version", $_POST)) {
	$merchantObj->SetVersion($_POST["version"]);
	unset($_POST["version"]);
}

// forme el request de la transacci�n
$request = $parserObj->ParseRequest($merchantObj, $_POST);


// En caso de no recibir un POST de la p�gina HTML (parseRequest devolver� "" al recibir un $_POST vac�o)
if ($request == "")
	die();

// imprime la solicitud antes del env�o al servidor en caso de est�r en modo debug
// esto �nicmente se utiliza para depurar. Esto no se debe utilizar en su integraci�n, ya que DEBUG debe establecerse como FALSE
if ($merchantObj->GetDebug())
	echo $request . "<br/><br/>";

// forma el requestUrl y lo asigna al miembro merchantObj gatewayUrl
// devuelve lo que estaba asignado al miembro gatewayUrl para imprimirlo si est� en modo de depuraci�n
$requestUrl = $parserObj->FormRequestUrl($merchantObj);

// esto �nicmente se utiliza para depurar. Esto no se debe utilizar en su integraci�n, ya que DEBUG debe establecerse como FALSE
if ($merchantObj->GetDebug())
	echo $requestUrl . "<br/><br/>";
	
// se realiza el intento de transacci�n
// $response se utiliza en la p�gina de recibo, no cambie nombre de la variable
$response = $parserObj->SendTransaction($merchantObj, $request);

// imprime la respuesta recibida del server
if ($merchantObj->GetDebug()) {
	// remplaza los caracteres de  newline chars con caracteres newlines de html 
	$response = str_replace("\n", "<br/>", $response);
	echo $response . "<br/><br/>";
//	die();
}

include "receipt.php";

?>