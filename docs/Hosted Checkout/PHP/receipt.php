<?php

$errorMessage = "";
$errorCode = "";
$gatewayCode = "";
$result = "";

$responseArray = array();

// significa que hay una cadena de Error en la respuesta del cURL
if (strstr($response, "cURL Error") != FALSE) {
  print("Fallo de Comunicaci�n. Por favor, revise la respuesta del servidor de pago (poner c�digo en modo de debug).");
  die();
}

// [Snippet] howToDecodeResponse - start
// recorra la respuesta del servidor y forme una matriz asociativa
// con el formato de par name/value
if (strlen($response) != 0) {
  $pairArray = explode("&", $response);
  foreach ($pairArray as $pair) {
    $param = explode("=", $pair);
    $responseArray[urldecode($param[0])] = urldecode($param[1]);
  }
}
// [Snippet] howToDecodeResponse - end

// [Snippet] howToParseResponse - start
if (array_key_exists("result", $responseArray))
  $result = $responseArray["result"];
  // [Snippet] howToParseResponse - end

// Forma una cadena de error si se activa alg�n error
if ($result == "FAIL") {
  if (array_key_exists("failureExplanation", $responseArray)) {
    $errorMessage = rawurldecode($responseArray["failureExplanation"]);
  }
  else if (array_key_exists("supportCode", $responseArray)) {
    $errorMessage = rawurldecode($responseArray["supportCode"]);
  }
  else {
    $errorMessage = "Reason unspecified.";
  }

  if (array_key_exists("failureCode", $responseArray)) {
    $errorCode = "Error (" . $responseArray["failureCode"] . ")";
  }
  else {
    $errorCode = "Error (SIN ESPECIFICAR)";
  }
}

else {
  if (array_key_exists("status", $responseArray))
    $gatewayCode = rawurldecode($responseArray["status"]);
  else
    $gatewayCode = "";
}

$_SESSION['successIndicator']= isset($responseArray['successIndicator']) ? $responseArray['successIndicator'] : '' ;

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <link rel="stylesheet" type="text/css" href="assets/paymentstyle.css" />
    <head>
      <title>Ejemplo HostedCheckout</title>
      <meta http-equiv="Content-Type" content="text/html, charset=UTF-8">
      <?php
	      	if(isset($responseArray['session.id'])){
	  ?>
      <script src="https://secure.na.tnspayments.com/checkout/version/30/checkout.js">
                data-error="errorCallback"
                data-cancel="cancelCallback">
      </script>
      
      <script type="text/javascript">
            function errorCallback(error) {
                  console.log(JSON.stringify(error));
            }
            function cancelCallback() {
                  console.log('Payment cancelled');
            }

            Checkout.configure({                
				session:{ 
					id: "<?php echo(isset($responseArray['session.id']) ? $responseArray['session.id']: ''); ?>"
				},
                order: {
                    description: 'Orden',
                },
                interaction: {
                    merchant: {
                        name: '[Incluya el nombre del comercio]',
                        address: {
                            line1: '[Domicilio y n�mero]',
                            line2: '[Poblaci�n, Estado y C�digo postal]'            
                        }    
                    }
				} 
            });
        </script>
	  <?php
      }
	  ?>
    
    </head>
    <body>

    <center><h1><br/>Ejemplo Hosted Checkout PHP NVP</h1></center>
    <center><h3>P&aacute;gina de Confirmaci&oacute;n</h3></center><br/><br/>

  <table width="80%" align="center" cellpadding="5" border="0">

  <?php
    // mostrar los encabezados de Error si se encuentra alg�n error
    if ($errorCode != "" || $errorMessage != "") {
  ?>
      <tr class="title">
             <td colspan="2" height="25"><P><strong>&nbsp;Error</strong></P></td>
         </tr>
         <tr>
             <td align="right" width="50%"><strong><i><?=$errorCode?>: </i></strong></td>
             <td width="50%"><?=$errorMessage?></td>
         </tr>
  <?php
    }
    else 
    {
  ?>
  		<?php
    		if(isset($responseArray['session.id']))
    		{
    	?>	
    			<script languaje="javascript"> 
				Checkout.showLightbox(); 
				</script> 
		<!-- <input type="button" value="Pay with Lightbox" onclick="Checkout.showLightbox();"> -->
    			<tr class="title">
             		<td colspan="2" height="25"><P><strong>&nbsp;Su transacci&oacute;n ser&aacute; procesada. Por favor espere</strong></P></td>
         		</tr>
    	<?php
			}
    		else
    		{
		?>
      		<tr class="title">
            	<td colspan="2" height="25"><P><strong>&nbsp;<?=$gatewayCode?></strong></P></td>
         	</tr>
         	<tr>
            	<td align="right" width="50%"><strong><i>Resultado: </i></strong></td>
             	<td width="50%"><?=$gatewayCode?></td>
         	</tr>
         	<tr class="title">
            	<td colspan="2" height="20"><P><strong>&nbsp;Todos los campos de respuesta</strong></P></td>
         	</tr>
    		<?php
      			$shade = 0;

      			// Campos de salida: Valor HTML en los campos de respuesta en una tabla HTML
      			foreach ($responseArray as $field => $value) 
      			{
        			if ($shade % 2 == 0) 
        			{
          				print "<tr class=\"shade\">";
        			}
        			else
          				print "<tr>";
        			$shade = $shade + 1;
    		?>
            		<td align="right" width="50%"><strong><i><?=$field?>: </i></strong></td>
            		<td width="50%"><?=$value?></td>
         		</tr>
         	<?php
     			}
  			?>
    	<?php
      		}
    	?>
   <?php
	}
   ?>
  </table>

  <br/><br/>
   </body>
</html>